<?php
    header("Access-Control-Allow-Origin: *");
    header("Content-Type: application/json; charset=UTF-8");
    header("Access-Control-Allow-Methods: POST");
    header("Access-Control-Max-Age: 3600");
    header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
    // include database and object files
    include_once '../config/database.php';
    include_once '../objects/users.php';

    $obj= new database;
    $connection=$obj->connectToDB();
    $created=new users($connection);
    $jsonInput = json_decode(file_get_contents("php://input"));
    $created->email=$jsonInput->email;
    $created->password=$jsonInput->password;
    $allTasks=$created->checkUsers();
    if($allTasks->num_rows>0){
        while($row=$allTasks->fetch_assoc()){
            extract($row);
            $name=$userName;
        }
        echo json_encode(array(
            "message"=>"Account Found",
            "name"=>$name
        ));
    }else{
        echo '{';
            echo '"message": "Account not found"';
        echo '}';
    }
?>