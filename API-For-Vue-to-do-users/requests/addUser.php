<?php
    header("Access-Control-Allow-Origin: *");
    header("Content-Type: application/json; charset=UTF-8");
    header("Access-Control-Allow-Methods: POST");
    header("Access-Control-Max-Age: 3600");
    header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
    // including database and product object files
    include_once '../config/database.php';
    include_once '../objects/users.php';
     $obj= new database;
     $connection=$obj->connectToDB();
     $created=new users($connection);
     //enable this function to create table
     //$created->createTable();
     $jsonInput = json_decode(file_get_contents("php://input"));
     $created->userName=$jsonInput->userName;
     $created->name=$jsonInput->name;
     $created->email=$jsonInput->email;
     $created->createdAt=date("Y-m-d H:i:s");
     $created->password=$jsonInput->password;
     if($created->createUsers()){
        echo json_encode(array(
            "message"=>"created successfully"
        ));
     }else{
        echo json_encode(array(
            "Error"=>"Not created"
        ));
     }
?>