<?php
    header("Access-Control-Allow-Origin: *");
    header("Content-Type: application/json; charset=UTF-8");
    header("Access-Control-Allow-Methods: POST");
    header("Access-Control-Max-Age: 3600");
    header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
    // including database and product object files
    include_once '../config/database.php';
    include_once '../objects/task.php';
     $obj= new database;
     $connection=$obj->connectToDB();
     $created=new task($connection);
     $created->taskId=isset($_GET['taskId']) ? $_GET['taskId'] : die();

     if($created->deleatTask()){
        echo json_encode(array(
            "message"=>"Deleted successfully"
        ));
     }else{
        echo json_encode(array(
            "Error"=>"Not Deleted"
        ));
     }
?>