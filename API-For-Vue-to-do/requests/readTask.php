<?php
    header("Access-Control-Allow-Origin: *");
    header("Content-Type: application/json; charset=UTF-8");
     
    // include database and object files
    include_once '../config/database.php';
    include_once '../objects/task.php';

    $obj= new database;
    $connection=$obj->connectToDB();
    $created=new task($connection);
    $created->userName=isset($_GET['userName']) ? $_GET['userName'] : die();
    $created->dueDate=date("Y-m-d");
    $allTasks=$created->readTasks();
    if($allTasks->num_rows>0){
        $taskList=array();
        $taskList['tasks']=array();
        while($row=$allTasks->fetch_assoc()){
            extract($row);
            $tasks=array(
                "taskId"=>$taskId,
                "userName"=>$userName,
                "taskName"=>$taskName,
                "taskDescription"=>$taskDescription,
                "createdAt"=>$createdAt,
                "dueDate"=>$dueDate,
                "dueTime"=>$dueTime
            );
            array_push($taskList['tasks'],$tasks);
        }
        echo json_encode($taskList);
    }else{
        echo '{';
            echo '"Error": "Does not have any task for this user today."';
        echo '}';
    }
?>