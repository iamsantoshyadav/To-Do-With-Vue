<?php
    class task {
        private $connect ='';
        private $tableName='tasks';
        //some public data for columns values
        public $userName='';
        public $taskName='';
        public $taskDescription='';
        public $dueDate ='';
        public $dueTime='';
        public $createdAt='';
        public $taskId='';

        public function __construct($connection){
            $this->connect=$connection;
        }
        //for create a  table
        public function createTable(){
            $sqlQuery="CREATE TABLE `$this->tableName` ( userName varchar(20) not null, taskName varchar(100) not null, taskId int AUTO_INCREMENT, taskDescription varchar(500), createdAt DATE, dueDate DATE, dueTime TIME, PRIMARY KEY (taskId) )";
            if($this->connect->query($sqlQuery)){
                echo 'successfully created';
            }else{
                echo "Error : ".$this->connect->error;
            }
        }
        //Now all these function are the methods for create and read the post
         public function createTask(){
             $sqlQuery="INSERT INTO `$this->tableName` (userName, taskName, taskDescription, createdAt, dueDate, dueTime) VALUES ('$this->userName','$this->taskName','$this->taskDescription','$this->createdAt','$this->dueDate','$this->dueTime')";
             if($this->connect->query($sqlQuery)){
                 return true;
             }else{
                 echo "Error : ".$this->connect->error;
                 return false;
             }
         }
         public function readTasks(){
             $sqlQuery="SELECT * FROM `$this->tableName` WHERE userName='$this->userName' AND dueDate='$this->dueDate'";
             if($this->connect->query($sqlQuery)){
                 $results=$this->connect->query($sqlQuery);
                return $results;
             }
             else{
                 echo "Error : ".$this->connect->error;
             }
         }
         //deleat task functioning
         public function deleatTask(){
            $sqlQuery="DELETE  FROM `$this->tableName` WHERE taskId='$this->taskId'";
            if($this->connect->query($sqlQuery)){
               return true;
            }
            else {
                return false; 
            }
         }
    }
?>
