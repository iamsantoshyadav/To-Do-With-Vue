/* eslint-disable */ 
/* eslint-disable no-new */
import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export const userDetails = new Vuex.Store({
    state:{
        status: {
            registeredSuccessfully: false,
            userName: '',
            guestUser: true,
            loginStatus: false
        }
    }

})
