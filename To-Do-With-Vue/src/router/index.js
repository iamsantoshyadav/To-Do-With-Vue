/* eslint-disable */
import Vue from 'vue'
import Router from 'vue-router'
//import HelloWorld from '@/components/HelloWorld'
import addTasks from '../components/addTasks.vue'
import allTasks from '../components/allTasks.vue'
import home from '../components/home.vue'
import register from '../components/register.vue'
import login from '../components/login.vue'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: home
    },
    {
      path: '/allTaskS',
      name: 'addyourtasks',
      component: allTasks
    },
    {
      path: '/register',
      name: 'register',
      component: register
    },
    {
      path: '/login',
      name: 'login',
      component: login
    }
  ]
})
