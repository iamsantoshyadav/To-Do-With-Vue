/* eslint-disable */ 
/* eslint-disable no-new */
// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import BootstrapVue from 'bootstrap-vue'
import "bootstrap/dist/css/bootstrap.min.css"
import "bootstrap-vue/dist/bootstrap-vue.css"
import header from './components/header.vue'
import VueResource from 'vue-resource'
import home from './components/home.vue'
import register from './components/register.vue'
import login from './components/login.vue'
import VeeValidate from 'vee-validate'
import addTasks from './components/addTasks.vue'
import {userDetails} from './store/users'

Vue.config.productionTip = false
Vue.use(BootstrapVue)
Vue.use(VueResource)
Vue.use(VeeValidate)

//using an event bus
 export const bus=new Vue();


Vue.component('app-header', header)
Vue.component('app-home', home)
Vue.component('app-register', register)
Vue.component('app-login', login)
Vue.component('add-Tasks', addTasks)
//Vue.component('all-Tasks', allTasks)
new Vue({
  store: userDetails,
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
